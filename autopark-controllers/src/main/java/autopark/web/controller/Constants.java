package autopark.web.controller;

/**
 * Created by aro on 07.03.2016.
 */
public class Constants {
    public static final String COMMAND = "command";
    public static final String USER = "user";

    public static final String MAIN = "main.jsp";

    public static final String SIGNUP = "signup.jsp";
    public static final String ENTER  = "enter.jsp";

    public static final String LOST_PASSWORD = "lostpassword.jsp";
    public static final String RECOVER_PASSWORD = "recover.jsp";


    private Constants() {
    }
}
