package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
public class CarController {
    @Autowired
    @Qualifier("carServiceHibernate")
    private ICarService carServiceHibernate;

    @RequestMapping(value = "/car/{id}")
    public String handle3(@PathVariable Long id, ModelMap modelMap){
        Date start = new Date();
        CarDTO car = carServiceHibernate.getCar(id);
        Date end = new Date();

        modelMap.put("millis",end.getTime()-start.getTime());
        modelMap.put("car", car);
        modelMap.put("author",System.getProperty("user.name"));
        return "/WEB-INF/content/car.jsp";
    }


}



