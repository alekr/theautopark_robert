package autopark.web.controller;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.IPasswordService;
import autopark.service.ITokenService;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;

/**
 * Created by  01 on 14.03.2016.
 */
@Controller
public class LostPasswordController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private IPasswordService passwordService;

    @Autowired
    @Qualifier("emailValidator")
    private Validator validator;


    @RequestMapping(value = "/lostpassword", method = RequestMethod.GET)
    public String lostPasswordGet(ModelMap modelMap) {
        return Constants.LOST_PASSWORD;
    }


    @RequestMapping(value = "/lostpassword", method = RequestMethod.POST)
    public String lostPasswordPost(@ModelAttribute("command") @Validated PasswordResetTokenDTO tokenDTO,
                             BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {


        if (bindingResult.hasErrors()) {
            modelMap.put(Constants.COMMAND, tokenDTO);
            return Constants.LOST_PASSWORD;
        }

        tokenDTO.setRemoteAddress(request.getRemoteAddr());
        tokenDTO.setUserAgent(request.getHeader("user-agent"));
        tokenDTO.setTokenDate(new Date());

        passwordService.requestResetPassword(tokenDTO, assembleBaseURL(request));

        return Constants.LOST_PASSWORD;
    }


    @RequestMapping(value = "/reset/finish", method = RequestMethod.GET)
    public String handleRecoverGet(Locale locale, ModelMap modelMap,
                                   @RequestParam(value = "key", required = false) String key,
                                   HttpServletRequest request) {

        /*https://stackoverflow.com/account/recover?recoveryToken=
        vIntVgAAAABZAQBzrF9PIg%3d%3d%7c1bfd1214c4e4f3dea8e2f2e47e5da77fbcf9c07836594c495c9f14491dec9482*/
        if(key != null){
            PasswordResetTokenDTO tokenDTO = tokenService.getPasswordResetTokenByTokenValue(key);
            if(!tokenDTO.isExpired()){ //Success
                modelMap.put("res", 0);
                return Constants.RECOVER_PASSWORD;
            }
        }

        modelMap.put("res", -2); // Fail
        return "redirect:/enter";

    }


    @RequestMapping(value = "/reset/finish",
            method = RequestMethod.POST)
    public String handleRecoverPost(@ModelAttribute("command") @Validated UserDTO userDTO,
                                    BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {


        if (bindingResult.hasErrors()) {
            return Constants.RECOVER_PASSWORD;
    }

        userDTO.setRemoteAddress(request.getRemoteAddr());
        /*userService.updateUserPassword(userDTO, assembleBaseURL(request));*/
        passwordService.finishResetPassword(userDTO, assembleBaseURL(request));

        return Constants.RECOVER_PASSWORD;// TODO
    }


    @InitBinder
    private void binder(WebDataBinder binder) {
        //Add validator
        binder.setValidator(validator);
    }

    private String assembleBaseURL(HttpServletRequest request) {
        return new StringBuilder().
                append(request.getScheme()).
                append("://").append(request.getServerName()).
                append(":").append(request.getServerPort()).
                append(request.getContextPath()).
                toString();
    }


}
