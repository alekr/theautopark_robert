package autopark.domain;

/**
 * Created by vclass19 on 08.02.2016.
 */
public enum CarLoadingCapacityEnum {
    BEFORE_1_5,
    BEFORE_3,
    BEFORE_5,
    BEFORE_7,
    BEFORE_10,
    BEFORE_12,
    BEFORE_13,
    BEFORE_15,
    BEFORE_20,
    ABOVE_20
}
