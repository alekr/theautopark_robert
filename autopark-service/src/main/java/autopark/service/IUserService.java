package autopark.service;

import autopark.dto.UserDTO;

public interface IUserService {
    boolean createUser(UserDTO dto, String baseURL);

    UserDTO getUserByEmail(String email);

    boolean updateUserPassword(UserDTO userDTO, String baseURL);

    void activateUser(String key);
}
