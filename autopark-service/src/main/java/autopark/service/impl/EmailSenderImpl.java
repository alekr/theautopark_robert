package autopark.service.impl;

import autopark.domain.User;
import autopark.dto.UserDTO;
import autopark.service.IEmailSender;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

//aupark@yandex.ru
//aupark!123 - password

//aupark2@gmail.com
//aupark!123 - password

@Service
@Qualifier("baseEmailSender")
public class EmailSenderImpl implements IEmailSender {

    private final Logger log = LoggerFactory.getLogger(EmailSenderImpl.class);


    @Autowired
    private JavaMailSenderImpl javaMailSender;

    @Autowired
    private SimpleMailMessage messageTemplate;

    @Autowired
    private SpringTemplateEngine templateEngine;


    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom("aupark@yandex.ru");
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent e-mail to User '{}'", to);
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
        }
    }
}