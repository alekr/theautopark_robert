package autopark.service.impl;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.IPasswordService;
import autopark.service.ISecurityEmailSender;
import autopark.service.ITokenService;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * 22.03.2016.
 */

@Service
public class PasswordServiceImpl implements IPasswordService {

    @Autowired
    private ISecurityEmailSender securityEmailSender;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;


    @Override
    @Transactional
    public void requestResetPassword(PasswordResetTokenDTO tokenDTO, String baseURL) {

        UserDTO userDTO = userService.getUserByEmail(tokenDTO.getEmail());
        //userDTO.setActivated(true);

        if (userDTO.isActivated()) {
            String tokenValue = passwordEncoder.encode(UUID.randomUUID().toString());

            tokenDTO.setTokenValue(tokenValue);
            tokenService.SavePasswordResetToken(tokenDTO);
            securityEmailSender.sendPasswordResetMail(baseURL, tokenDTO);
        }

    }

    @Override
    @Transactional
    public void finishResetPassword(UserDTO userDTO, String baseURL) {

        userService.updateUserPassword(userDTO, baseURL);
        securityEmailSender.sendSuccessPasswordResetMail(baseURL, userDTO);

    }
}
