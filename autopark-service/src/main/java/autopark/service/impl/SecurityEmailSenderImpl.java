package autopark.service.impl;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.ISecurityEmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;

import java.util.Locale;

/**
 * 22.03.2016.
 */
@Service
@Qualifier("SecurityEmailSender")
public class SecurityEmailSenderImpl extends EmailSenderImpl implements ISecurityEmailSender  {

    private final Logger log = LoggerFactory.getLogger(SecurityEmailSenderImpl.class);

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private MessageSource messageSource;

    @Async
    public void
    sendPasswordResetMail(String baseUrl, PasswordResetTokenDTO tokenDTO) {
        log.debug("Sending password reset e-mail to '{}'", tokenDTO.getEmail());
        Locale locale = Locale.forLanguageTag("en"); // TODO
        Context context = new Context(locale);
        context.setVariable("user", tokenDTO);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("passwordResetEmail.html", context);
        String subject = "Reset Subject";///messageSource.getMessage("email.reset.title", null, locale);
        sendEmail(tokenDTO.getEmail(), subject, content, false, true);
    }


    @Async
    public void sendActivationEmail(String baseUrl, UserDTO user) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("en"); //TODO
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("activationEmail.html", context);
        String subject = "Activation Subject";//messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendCreationEmail(String baseUrl, UserDTO user) {
        log.debug("Sending creation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("en"); //TODO
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("creationEmail.html", context);
        String subject = "Creation Subject";/*messageSource.getMessage("email.activation.title", null, locale);*/
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendSuccessPasswordResetMail(String baseURL, UserDTO user) {
        //TODO
    }

}
