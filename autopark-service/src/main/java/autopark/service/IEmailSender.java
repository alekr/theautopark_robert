package autopark.service;

import autopark.domain.User;
import autopark.dto.UserDTO;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IEmailSender {
    void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml);

}
