package autopark.service;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;

/**
 * 22.03.2016.
 */
public interface ISecurityEmailSender {

    void sendPasswordResetMail(String baseURL, PasswordResetTokenDTO tokenDTO);

    void sendSuccessPasswordResetMail(String baseURL, UserDTO user);

    void sendActivationEmail(String baseUrl, UserDTO user);

    void sendCreationEmail(String baseUrl, UserDTO user);
}
