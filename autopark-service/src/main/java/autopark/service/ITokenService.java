package autopark.service;

import autopark.dto.PasswordResetTokenDTO;

/**
 * 21.03.2016.
 */
public interface ITokenService {

    PasswordResetTokenDTO getPasswordResetTokenByTokenValue(String tokenValue);

    PasswordResetTokenDTO getPasswordResetTokenByEmail(String email);

    void SavePasswordResetToken(PasswordResetTokenDTO tokenDTO);

}
