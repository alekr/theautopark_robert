package autopark.repository;

import autopark.domain.PasswordResetToken;
import org.springframework.data.repository.CrudRepository;

/**
 * 21.03.2016.
 */
public interface PasswordResetTokenCrudRepository extends CrudRepository<PasswordResetToken, Long> {
}
