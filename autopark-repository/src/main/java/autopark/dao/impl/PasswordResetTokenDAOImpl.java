package autopark.dao.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.domain.PasswordResetToken;
import org.springframework.stereotype.Service;

/**
 * 21.03.2016.
 */
@Service
public class PasswordResetTokenDAOImpl extends RootDAOImpl<PasswordResetToken> implements IPasswordResetTokenDAO {

    public PasswordResetToken getToken(String token) {
        return null;
    }
}
