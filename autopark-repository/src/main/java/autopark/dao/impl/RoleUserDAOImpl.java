package autopark.dao.impl;

import autopark.dao.IRoleUserDAO;
import autopark.domain.RoleUser;
import org.springframework.stereotype.Repository;


@Repository
public class RoleUserDAOImpl extends RootDAOImpl<RoleUser> implements IRoleUserDAO {

    public RoleUserDAOImpl() {
        super("autopark.domain.RoleUser", RoleUser.class);
    }

}