package autopark.dao;

import autopark.domain.PasswordResetToken;
import autopark.dto.PasswordResetTokenDTO;

/**
 * 21.03.2016.
 */
public interface IPasswordResetTokenDAO extends IRootDAO<PasswordResetToken>{

    PasswordResetToken getToken(String token);
}
